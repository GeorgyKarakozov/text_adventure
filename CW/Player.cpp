#include "Player.h"
#pragma region Constructors
Player::Player() { //The unnamed player constructor
	movement = 0;
	room = 1;
	name = "";
	inventory[0] = "";
	inventory[1] = "";
	inventory[2] = "";
	inventory[3] = "";
	inventory[4] = "";
}
Player::Player(string n) { //The named player constructor
	movement = 0;
	room = 1;
	name = n;
	inventory[0] = "";
	inventory[1] = "";
	inventory[2] = "";
	inventory[3] = "";
	inventory[4] = "";
}
#pragma endregion
#pragma region Setters
void Player::IncrementMove() { //+1 to moves
	movement++;
}
void Player::SetName(string n) { //Sets the name
	name = n;
	cout << "Welcome to the game, " << name << endl;
}
void Player::AddInventory(string item) { //Adds an item to inventory
	for (int i = 0; i < 5; i++) {
		if (inventory[i] == "") {
			inventory[i] = item;
			cout << item << " has been added to your inventory" << endl;
			break;
		}
		else if(i == 4) {
			cout << "Inventory full!" << endl;
		}
	}
}
void Player::DeleteInventory(string item) { //Deletes an item from inventory
	for (int i = 0; i < 5; i++) {
		if (inventory[i] == item) {
			inventory[i] = "";
			cout << item << " has been deleted from your inventory" << endl;
			break;
		}
		else if (i = 4) {
			cout << "No such item in the inventory" << endl;
		}
	}
}
void Player::SetRoom(string r) { //Sets the room the player is in
	room = r;
}
#pragma endregion
#pragma region Getters
int Player::GetMove() { //Returns the moves
	return movement;
}
string Player::GetName() { //Returns the name
	return name;
}
string Player::GetInventory(int i) { //Returns item i in inventory
	return inventory[i];
}
void Player::ShowInventory() { //Prints entire inventory
	cout << "Your inventory is:" << endl;
	for (int i = 0; i < 5; i++) {
		if (inventory[i] == "") {
			cout << "Empty slot" << endl;
		}
		else cout << inventory[i] << endl;
	}
}
string Player::GetRoom() { //Returns the room number
	return room;
}
#pragma endregion