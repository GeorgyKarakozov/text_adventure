#pragma region Includes
#include <iostream>
#include "AdventureRoom.h"
#pragma endregion
#pragma region Constructors
AdventureRoom::AdventureRoom() {
	number = "0";
	visit = false;
	north = false;
	east = false;
	south = false;
	west = false;
	name = "";
	descr = "";
	items[0] = "";
	items[1] = "";
	items[2] = "";
	items[3] = "";
	items[4] = "";
	exits[0] = "";
	exits[1] = "";
	exits[2] = "";
	exits[3] = "";
}
#pragma endregion
#pragma region Setters
void AdventureRoom::SetNumber(string n) { //Set room number
	number = n;
}
void AdventureRoom::SetVisit(bool v) { //Set visit bool
	visit = v;
}
void AdventureRoom::SetN(bool n) { //Set nothern gate
	north = n;
}
void AdventureRoom::SetE(bool e) { //Set eastern gate
	east = e;
}
void AdventureRoom::SetS(bool s) { //Set southern gate
	south = s;
}
void AdventureRoom::SetW(bool w) { //Set western gate
	west = w;
}
void AdventureRoom::SetName(string n) { //Set room name
	name = n;
}
void AdventureRoom::SetDescr(string d) { //Set description
	descr = d;
}
void AdventureRoom::AddItems(string i) { //Add items
	for (int y = 0; y < 5; y++) {
		if (items[y] == ""){
			items[y] = i;
			break;
		}
	}
}
void AdventureRoom::DeleteItems(int i) { //Delete items
	items[i] = "";
}
void AdventureRoom::SetExits(string e[4]) { //Set exits
	exits[0] = e[0];
	exits[1] = e[1];
	exits[2] = e[2];
	exits[3] = e[3];

}
#pragma endregion
#pragma region Getters
string AdventureRoom::GetNumber() {//Returns room number
	return number;
}
bool AdventureRoom::GetVisit() { //Returns visit bool
	return visit;
}
bool AdventureRoom::GetN() {//Returns northern gate
	return north;
}
bool AdventureRoom::GetE() { //Returns eastern gate
	return east;
}
bool AdventureRoom::GetS() { //Returns southern gate
	return south;
}
bool AdventureRoom::GetW() { //Returns western gate
	return west;
}
string AdventureRoom::GetName() { //Returns room name
	return name;
}
string AdventureRoom::GetDescr() { //Returns description
	return descr;
}
string AdventureRoom::GetItem(int i) { //Returns item i
	return items[i];
}
#pragma endregion
#pragma region Prints
void AdventureRoom::ShowItems() { //Prints all items
	cout << "The items are:";
	for (int i = 0; i < 5; i++) {
		if (items[i] != "") {
			cout << "\n" << items[i];
		}
	}
	cout << endl;
}
void AdventureRoom::ShowExits() { //Prints all exits
	for (int i = 0; i < 4; i++) {
		if (exits[i] != "") {
			cout << "\n" << exits[i];
		}
	}
	cout << endl;
}
void AdventureRoom::PrintRoom() { //Prints entire room
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	cout << GetNumber() << endl;
	cout << GetName() << endl;
	cout << GetDescr() << endl;
	if (visit) {
		cout << "You have been here!" << endl;
	}
	else cout << "You haven't been here" << endl;
	ShowItems();
	ShowExits();
}
#pragma endregion

