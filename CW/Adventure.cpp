#include "Adventure.h"
#pragma region Constructors
Adventure::Adventure(string f) { //Fetches the file
	filename = f;
}
#pragma endregion
#pragma region Getters
string Adventure::GetFile() { //
	return filename;
}
#pragma endregion
void Adventure::Read() { //The longest function in this coursework. Reads the file assinging right value to right variables
	ifstream ReadFile; //File reader
	string temp; //this will be the paper
	string d = ""; //separate description variable
	string exits[4] = { "", "", "", "" }; //separate exits array
	ReadFile.open(filename); //opens the file
	if (!ReadFile.is_open()) { //Error checking
		cout << "File failed to open!" << endl;
	}
#pragma region Room 1
	getline(ReadFile, temp);
	room1->SetNumber(temp);
	getline(ReadFile, temp);
	room1->SetName(temp);
	getline(ReadFile, temp);
	d += temp;
	d.push_back('\n');
	getline(ReadFile, temp);
	d += temp;
	d.push_back('\n');
	getline(ReadFile, temp);
	d += temp;
	d.push_back('\n');
	room1->SetDescr(d);
	d = "";
	getline(ReadFile, temp);
	exits[0] = temp;
	getline(ReadFile, temp);
	exits[1] = temp;
	getline(ReadFile, temp);
	exits[2] = temp;
	getline(ReadFile, temp);
	exits[3] = temp;
	room1->SetExits(exits);
	getline(ReadFile, temp);
	room1->AddItems(temp);
	getline(ReadFile, temp);
	room1->AddItems(temp);
	getline(ReadFile, temp);
	room1->AddItems(temp);
	getline(ReadFile, temp);
	room1->AddItems(temp);
	getline(ReadFile, temp);
	room1->AddItems(temp);
	for (int i = 0; i < 5; i++){
		if (player->GetInventory(i) == "Key") {
			room1->SetN(true);
			break;
		}
	}
	room1->SetE(true);
	for (int i = 0; i < 5; i++) {
		if (player->GetInventory(i) == "Lever") {
			room1->SetS(true);
			break;
		}
	}
	room1->SetW(true);
#pragma endregion
#pragma region Room 2
	getline(ReadFile, temp);
	getline(ReadFile, temp);
	room2->SetNumber(temp);
	getline(ReadFile, temp);
	room2->SetName(temp);
	getline(ReadFile, temp);
	d += temp;
	d.push_back('\n');
	getline(ReadFile, temp);
	d += temp;
	d.push_back('\n');
	getline(ReadFile, temp);
	d += temp;
	d.push_back('\n');
	room2->SetDescr(d);
	d = "";
	getline(ReadFile, temp);
	exits[0] = temp;
	getline(ReadFile, temp);
	exits[1] = temp;
	getline(ReadFile, temp);
	exits[2] = temp;
	getline(ReadFile, temp);
	exits[3] = temp;
	room2->SetExits(exits);
	getline(ReadFile, temp);
	room2->AddItems(temp);
	getline(ReadFile, temp);
	room2->AddItems(temp);
	getline(ReadFile, temp);
	room2->AddItems(temp);
	getline(ReadFile, temp);
	room2->AddItems(temp);
	getline(ReadFile, temp);
	room2->AddItems(temp);
	room2->SetN(false);
	room2->SetE(false);
	room2->SetS(false);
	room2->SetW(true);
#pragma endregion
#pragma region Room 3
	getline(ReadFile, temp);
	getline(ReadFile, temp);
	room3->SetNumber(temp);
	getline(ReadFile, temp);
	room3->SetName(temp);
	getline(ReadFile, temp);
	d += temp;
	d.push_back('\n');
	getline(ReadFile, temp);
	d += temp;
	d.push_back('\n');
	getline(ReadFile, temp);
	d += temp;
	d.push_back('\n');
	room3->SetDescr(d);
	d = "";
	getline(ReadFile, temp);
	exits[0] = temp;
	getline(ReadFile, temp);
	exits[1] = temp;
	getline(ReadFile, temp);
	exits[2] = temp;
	getline(ReadFile, temp);
	exits[3] = temp;
	room3->SetExits(exits);
	getline(ReadFile, temp);
	room3->AddItems(temp);
	getline(ReadFile, temp);
	room3->AddItems(temp);
	getline(ReadFile, temp);
	room3->AddItems(temp);
	getline(ReadFile, temp);
	room3->AddItems(temp);
	getline(ReadFile, temp);
	room3->AddItems(temp);
	room3->SetN(true);
	room3->SetE(false);
	room3->SetS(false);
	room3->SetW(false);
#pragma endregion
#pragma region Room 4
	getline(ReadFile, temp);
	getline(ReadFile, temp);
	room4->SetNumber(temp);
	getline(ReadFile, temp);
	room4->SetName(temp);
	getline(ReadFile, temp);
	d += temp;
	d.push_back('\n');
	getline(ReadFile, temp);
	d += temp;
	d.push_back('\n');
	getline(ReadFile, temp);
	d += temp;
	d.push_back('\n');
	room4->SetDescr(d);
	d = "";
	getline(ReadFile, temp);
	exits[0] = temp;
	getline(ReadFile, temp);
	exits[1] = temp;
	getline(ReadFile, temp);
	exits[2] = temp;
	getline(ReadFile, temp);
	exits[3] = temp;
	room4->SetExits(exits);
	getline(ReadFile, temp);
	room4->AddItems(temp);
	getline(ReadFile, temp);
	room4->AddItems(temp);
	getline(ReadFile, temp);
	room4->AddItems(temp);
	getline(ReadFile, temp);
	room4->AddItems(temp);
	getline(ReadFile, temp);
	room4->AddItems(temp);
	room4->SetN(false);
	room4->SetE(true);
	room4->SetS(false);
	room4->SetW(false);
#pragma endregion
#pragma region Room 5
	getline(ReadFile, temp);
	getline(ReadFile, temp);
	room5->SetNumber(temp);
	getline(ReadFile, temp);
	room5->SetName(temp);
	getline(ReadFile, temp);
	d += temp;
	d.push_back('\n');
	getline(ReadFile, temp);
	d += temp;
	d.push_back('\n');
	getline(ReadFile, temp);
	d += temp;
	d.push_back('\n');
	room5->SetDescr(d);
	d = "";
	getline(ReadFile, temp);
	exits[0] = temp;
	getline(ReadFile, temp);
	exits[1] = temp;
	getline(ReadFile, temp);
	exits[2] = temp;
	getline(ReadFile, temp);
	exits[3] = temp;
	room5->SetExits(exits);
	getline(ReadFile, temp);
	room5->AddItems(temp);
	getline(ReadFile, temp);
	room5->AddItems(temp);
	getline(ReadFile, temp);
	room5->AddItems(temp);
	getline(ReadFile, temp);
	room5->AddItems(temp);
	getline(ReadFile, temp);
	room5->AddItems(temp);
	room5->SetN(false);
	room5->SetE(false);
	room5->SetS(true);
	room5->SetW(false);
#pragma endregion
	ReadFile.close(); //Closes the file
}
#pragma region Movers
void Adventure::MoveN() {
	if ((room1->GetN() == true)&&(player->GetRoom() == "1")) {
		player->SetRoom("5");
		player->IncrementMove();
		room5->PrintRoom();
		MainLoop();
	}
	else if (player->GetRoom() == "3") {
		room3->SetVisit(true);
		player->SetRoom("1");
		player->IncrementMove();
		room1->PrintRoom();
		MainLoop();
	}
	else {
		cout << "You cannot go there" << endl;
		MainLoop();
	}
}
void Adventure::MoveE() {
	if (player->GetRoom() == "1") {
		player->SetRoom("2");
		player->IncrementMove();
		room2->PrintRoom();
		MainLoop();
	}
	else if (player->GetRoom() == "4") {
		room4->SetVisit(true);
		player->SetRoom("1");
		player->IncrementMove();
		room1->PrintRoom();
		MainLoop();
	}
	else { 
		cout << "You cannot go there" << endl; 
		MainLoop();
	}
}
void Adventure::MoveW() {
	if (player->GetRoom() == "1") {
		player->SetRoom("4");
		player->IncrementMove();
		room4->PrintRoom();
		MainLoop();
	}
	else if (player->GetRoom() == "2") {
		room2->SetVisit(true);
		player->SetRoom("1");
		player->IncrementMove();
		room1->PrintRoom();
		MainLoop();
	}
	else {
		cout << "You cannot go there" << endl;
		MainLoop();
	}
}
void Adventure::MoveS() {
	if ((room1->GetS() == true) && (player->GetRoom() == "1")) {
		player->SetRoom("3");
		player->IncrementMove();
		room3->PrintRoom();
		MainLoop();
	}
	else if (player->GetRoom() == "5") {
		room5->SetVisit(true);
		player->SetRoom("1");
		player->IncrementMove();
		room1->PrintRoom();
		MainLoop();
	}
	else {
		cout << "You cannot go there" << endl;
		MainLoop();
	}
}
#pragma endregion
#pragma region Item Interactions
void Adventure::Take() { //Pick up an item
	string temp;
	if ((player->GetRoom() == "1")&&(room1->GetItem(0) != "")) {
		temp = room1->GetItem(0);
		room1->DeleteItems(0);
		player->AddInventory(temp);
	}
	else if ((player->GetRoom() == "2")&&(room2->GetItem(0) != "")) {
		temp = room2->GetItem(0);
		room1->DeleteItems(0);
		player->AddInventory(temp);
	}
	else if ((player->GetRoom() == "3")&&(room3->GetItem(0) != "")) {
		temp = room3->GetItem(0);
		room1->DeleteItems(0);
		player->AddInventory(temp);
	}
	else if ((player->GetRoom() == "4")&&(room4->GetItem(0) != "")) {
		temp = room4->GetItem(0);
		room1->DeleteItems(0);
		player->AddInventory(temp);
	}
	else if ((player->GetRoom() == "5")&&(room5->GetItem(0) != "")) {
		temp = room5->GetItem(0);
		room1->DeleteItems(0);
		player->AddInventory(temp);
	}
	else {
		cout << "There are no items to take!" << endl;
	}
	MainLoop();
	
}
void Adventure::Drop() { //Drop an item
	string temp;
	cout << "Please enter the name of the item you want to drop (It will be lost forever): ";
	cin >> temp;
	for (int i = 0; i < 5; i++) {
		if (player->GetInventory(i) == temp) {
			player->DeleteInventory(temp);
			cout << "You have dropped " << temp << endl;
		}
	}
	MainLoop();
}
#pragma endregion
void Adventure::Look() { //Reprints the room
	if (player->GetRoom() == "1") {
		room1->PrintRoom();
		MainLoop();
	}
	else if (player->GetRoom() == "2") {
		room2->PrintRoom();
		MainLoop();
	}
	else if (player->GetRoom() == "3") {
		room3->PrintRoom();
		MainLoop();
	}
	else if (player->GetRoom() == "4") {
		room4->PrintRoom();
		MainLoop();
	}
	else if (player->GetRoom() == "5") {
		room5->PrintRoom();
		MainLoop();
	}

}
void Adventure::Victory() { //Victory screen with some stats
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	cout << "Congratulations, " << player->GetName() << ", from the rope and the hook you crafted a grappling hook and escaped the dungeon! Victory!" << endl;
	cout << "You have finished in " << player->GetMove() << " moves!" << endl;
	cout << "Type anything to quit: ";
	int temp;
	cin >> temp;
	exit(0);
}
void Adventure::MainLoop() { //Main Menu
	bool rope = false, hook = false; //Victory conditions check
	for (int i = 0; i < 5; i++) {
		if (player->GetInventory(i) == "Key") {
			room1->SetN(true);
		}
		if (player->GetInventory(i) == "Lever") {
			room1->SetS(true);
		}
		if (player->GetInventory(i) == "Rope") rope = true;
		if (player->GetInventory(i) == "Hook") hook = true;
	}
	if ((rope == true) && (hook == true)) {
		victory = true;
	}
	if ((victory) && (player->GetRoom() == "1")) Victory(); //If true goes straight to victory
	string choice = ""; //The menu itself
	cout << endl;
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	cout << "You have following options: " << endl;
	cout << "NORTH to take the way north" << endl;
	cout << "SOUTH to take the way south" << endl;
	cout << "EAST to take the way east" << endl;
	cout << "WEST to take the way west" << endl;
	cout << "INTERACT to interact with what is in the room, like taking object and flipping levers" << endl;
	cout << "LOOK to print the room details again" << endl;
	cout << "INVENTORY to see your inventory" << endl;
	cout << "DROP to drop an item by name" << endl;
	cout << "HELP to print this again" << endl;
	cout << "QUIT to quit" << endl;
	cout << "Enter your choice exactly as instructed: ";
	cin >> choice;
	if (choice == "NORTH") MoveN();
	else if (choice == "SOUTH") MoveS();
	else if (choice == "EAST") MoveE();
	else if (choice == "WEST") MoveW();
	else if (choice == "INTERACT") Take();
	else if (choice == "LOOK") Look();
	else if (choice == "INVENTORY") {
		player->ShowInventory();
		MainLoop();
	}
	else if (choice == "DROP") Drop();
	else if (choice == "HELP") MainLoop();
	else if (choice == "QUIT") exit(0);
	else { //Error check
		cout << "You have entered an invalid answer!" << endl;
		MainLoop();
	}
}