#pragma region Includes
#ifndef PLAYER_H
#define PLAYER_H
#include <iostream>
#include <string>

using namespace std;

#pragma endregion
class Player {
#pragma region Private Variable Declarations
private:
	int movement; //How many times the player moved
	string room; //Room you are in
	string name; //Your very name
	string inventory[5] = {}; //Inventory
#pragma endregion
#pragma region Public Functions Declarations
public:
	Player(); //Constructors
	Player(string n); //Constructors
	void IncrementMove(); //+1 to moves
	void SetName(string n); //Set your name
	void AddInventory(string item); //Add items to your inventory
	void DeleteInventory(string item); //Delete items from your inventory
	void SetRoom(string r); //Move the player to a room
	int GetMove(); //Returns the amount of moves
	string GetName(); //Returns the name
	string GetInventory(int i); //Returns the i item in the inventory(needed for searching)
	void ShowInventory(); //Prints the entire inventory
	string GetRoom(); //Returns the room you are in
#pragma endregion
};
#endif 
