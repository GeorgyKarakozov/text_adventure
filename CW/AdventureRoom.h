#pragma region Includes
#ifndef ADVENTUREROOM_H
#define ADVENTUREROOM_H
#include <iostream>
#include <string>
#include "Player.h"

using namespace std;
#pragma endregion
class AdventureRoom {
#pragma region Private Variables Declarations
private:
	string number; //Room number
	bool visit, north, east, south, west; //Been visited and whether the exits are open
	string name, descr; //Room title and description
	string items[5] = {}; //Items in the room
	string exits[4] = {}; //Exits from the room
#pragma endregion
#pragma region Public Functions Declarations
public:
	AdventureRoom(); //Constructor
	void SetNumber(string n); // Sets the room number
	void SetVisit(bool v); //Sets whether the room has been visited
	void SetName(string n); //Sets room's name
	void SetDescr(string d); //Sets the description
	void SetN(bool n); //locks/unlocks northern door
	void SetE(bool e); //locks/unlocks eastern door
	void SetS(bool s); //locks/unlocks southern door
	void SetW(bool w); //locks/unlocks western door
	void AddItems(string i); //Adds items to the room
	void DeleteItems(int i); //Deletes items from the room
	void SetExits(string e[4]); //Sets up the exits
	string GetNumber(); //Returns room number
	bool GetVisit(); //Returns the visit bool
	bool GetN(); //Returns northern lock
	bool GetE(); //Returns eastern lock
	bool GetS(); //Returns southern lock
	bool GetW(); //Returns western lock
	string GetName(); //Returns room name
	string GetDescr(); //Returns description
	string GetItem(int i); //Returns item i from the room
	void ShowItems(); //Prints all items
	void ShowExits(); //Prints all exits
	void PrintRoom(); //Prints the entire room
#pragma endregion
};
#endif // !ADVENTUREROOM_H
