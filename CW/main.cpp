#pragma region Includes
#include <iostream>
#include <string>
#include <Windows.h>
#include "Player.h"
#include "Adventure.h"
#include "AdventureRoom.h"
using namespace std;
#pragma endregion

int main(int arg, char* argv[]) {
	SetConsoleTitle(TEXT("Text Adventure"));
#pragma region Welcome Pack
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	cout << "Welcome to the adventure!" << endl;
	cout << "Enter the filename for the adventure (test.txt): ";
	string temp;
	cin >> temp;
	Adventure *adventure = new Adventure(temp);
	adventure->Read();
	cout << "Enter your name, traveller: ";
	cin >> temp;
	adventure->player->SetName(temp);
#pragma endregion
#pragma region Putting Player in the dungeon
	adventure->player->SetRoom(adventure->room1->GetNumber()); //Puts the player in room 1
	adventure->room1->PrintRoom(); //Prints the contents of room 1
	adventure->room1->SetVisit(true); //And makes us visit the room
	adventure->MainLoop(); //Prints the menu
#pragma endregion
	system("PAUSE");
	return 0;
}