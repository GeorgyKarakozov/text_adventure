#pragma region Includes
#ifndef ADVENTURE_H
#define ADVENTURE_H

#include <iostream>
#include <string>
#include <fstream>
#include "Player.h"
#include "AdventureRoom.h"

using namespace std;
#pragma endregion
class Adventure {
#pragma region Private Variables Declarations
private:
	string filename; //Name of the data file
	bool victory; //When true you won
#pragma endregion
#pragma region Public Function Declarations
public:
	Adventure(string f); //Constructor
	string GetFile(); //Returns the file name
	void Read(); //Reads the file
	void MainLoop(); //Menu
	void MoveN(); //Move north
	void MoveE(); //Move east
	void MoveS(); //Move south
	void MoveW(); //Move west
	void Take(); //Take items
	void Look(); //Reprint room details
	void Drop(); //Dispose an item
	void Victory(); //Victory text
#pragma region Room Declarations
	AdventureRoom *room1 = new AdventureRoom();
	AdventureRoom *room2 = new AdventureRoom();
	AdventureRoom *room3 = new AdventureRoom();
	AdventureRoom *room4 = new AdventureRoom();
	AdventureRoom *room5 = new AdventureRoom();
#pragma endregion
	Player *player = new Player(); // Creates a player
#pragma endregion
};
#endif // !ADVENTURE_H

